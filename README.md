# Lllog Component
The Lllog component is designed to store system execution logs according to user-defined specifications. Logs are recorded every time the component is called, providing a real history of all desired steps.

## Usage
There are two ways to use lllogs: the simplified way and the advanced way. Both require initializing and passing initial configurations to the object.
```php
$log = new Logger();
$log->setApp('{app name}');
$log->setModule('{module name}');
$log->setAuthor('{author name}');
```
* **app**: Refers to the application, if logging in an application named "Virtual Store," the app attribute should be set to **virtualStore**.
  * Example: ``$log->setApp('VirtualStore');``

* **module**: Represents the module where the log is being recorded. For example, if the *Virtual Store* application is logging *Products*, then **products** is suggested as the module.
  * Example: ``$log->setModule('products');``

* **author**: Assigned to the user using the system.
  * Example: ``$log->setAuthor('Leonidas');``

### Simple Usage
In the simple usage, developers only need to use one of the recording methods <br>
These can be: ``info|success|error|warning|debug``

* **type**: The log type can be info, success, error, warning, or debug.
  * Example: ``$log->[info|success|error|warning|debug]();``
  * It can also be changed later with the changeStatus() method, using ``$returnLog->changeStatus(Logger::[info|success|error|warning|debug]);``

Use it as follows:<br>
``$log->info(string {message}, string {detail}, string {key}, string|boolean {expire}, string {jsonData}, string {alias} )``
* **message**: The main text of the log, which can also be its title.
* **detail**: Information about the log execution can be recorded here.
* **key**: A parameter identifying to whom this log belongs, such as an ID or reference.
* **expire**: Defines the duration of this log entry. It can be passed as a DateInterval() notation or as a boolean. If false, it has a duration of three months (P3M), and if true, 14 days (P14D).
* **jsonData**: Can include a JSON of data related to the recorded log, which can be used for reports.
* **alias**: This field is responsible for aliasing the log entry, which can be consulted and used in reports.

#### Non-manipulable Fields

* **token**: Generates a token for each instantiated object. All recordings using this object will receive the same token.
* **datetime**: Records the datetime of the log recording.

### Advanced Usage
In this usage, it is possible to modify a log entry, recording more information and changing its type, among other functions.<br>
To do this, after recording a log, return its content to a variable. This variable is an object of that log entry, e.g., ```$info = $log->info('Test log');```.<br>
After that, you can use some methods to edit this entry.

* **step()**: With this method, you will insert records into the *data* field (the same as detail). You can call it several times, and the information passed will accumulate in the log entry.
* **changeStatus()**: This method changes the log type, and it can be ``Logger::[INFO|SUCCESS|ERROR|WARNING|DEBUG]``.
* **key()**: With the key() method, you can inform the key of the entry.
* **alias()**: Using the alias() method, you can specify an alias for the entry you are working on.
* **expire()**: In the expire() method, the developer can specify the period during which the entry will be available, similar to the *expire* parameter, which can be a DateInterval() or a boolean.
* **jsonData()**: In this method, the developer can specify a JSON for the entry, which can be used in future reports.

> The above methods can be used in a chain before defining the log entry.<br>
> Example: ``$log->key('21')->step('The product was successfully registered in the database')->success('Recording product');`` <br>
> In this case, always use the recording method {info|success|error|warning|debug}() as the last in the sequence.

## stash() Method
The stash() method can be used to store a series of information that will be unloaded into the data/detail of the log as soon as a recording occurs.<br>
Example:
```php
$log->stash('Building address');
$log->stash('Clearing phone');
$log->info('Sending data', 'Data sent: {...}', 22);
```

## Usage Example

```php
$log = new Logger();
$log->setApp('virtualStore');
$log->setModule('products');
$log->setAuthor('Leonidas');

// Simple usage mode
$log->stash('stash test'); 
$log->info('bla bla bla', 'info test one', null, false, 1); 

// Advanced usage mode
$log->stash('stash test'); 
$info = $log->key('365984')->info('Test log');
$info->step('prepare data');
$info->changeStatus(Logger::SUCCESS)->step('Successfully changed');
```