<?php

namespace LlLog\Models;

use LliureCore\Model;
use dibi;

class Logs extends Model
{
    protected static $primaryKey = 'id';
	protected static ?string $autoIncrementColumn = 'id';
    protected static ?string $table = 'lllogs_logs';
	protected static array $fields = ['id', 'type', 'datetime', 'app', 'module', 'key', 'alias', 'log', 'data', 'jsonData', 'token', 'author', 'expireAt'];
	protected static array $jsonFields = ['jsonData'];
	
	protected static $expiredInterval = 'P7D';
	protected static $OldDeleteInterval = 'P2M';
	
    /**
     * @param $key
     * @return \LliureCore\Collection
     * @throws \Exception
     */
    static function findByKey($key)
    {
        $table = static::getTable();
        return static::findMany('select * from ' . $table . ' where `key` =?', $key);
    }

    /**
     * @return int
     * @throws \Exception
     */
    static function deleteExpired()
    {
        $dateExpired = new \DateTime();

        $table = static::getTable();
        $return = dibi::query('DELETE FROM ' . $table . ' WHERE expireAt < ?', $dateExpired);

        return $return->count();
    }
}