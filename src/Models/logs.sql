CREATE TABLE `ll_lllogs_logs` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`type` ENUM('success','error','info','warning','debug') NULL DEFAULT 'info',
	`datetime` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
	`app` VARCHAR(50) NOT NULL,
	`module` VARCHAR(50) NOT NULL,
	`key` VARCHAR(50) NULL DEFAULT NULL,
	`alias` VARCHAR(50) NULL DEFAULT NULL COMMENT 'Apelido para filtros',
	`log` VARCHAR(250) NOT NULL COMMENT 'short description',
	`data` LONGTEXT NULL COMMENT 'full description',
    `jsonData` TEXT NULL,
	`token` VARCHAR(100) NOT NULL COMMENT 'token de linearidade',
	`author` VARCHAR(100) NULL DEFAULT 'Anonymous',
	`expireAt` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;
