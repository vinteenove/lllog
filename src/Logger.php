<?php

namespace LlLog;

use LlLog\Models\Logs;

class Logger
{
	const DATE_FORMAT = "Y-m-d H:i:s.u";
	
	public $module;
	public $author = false;
	public $app    = null;
	
	private float $startTime;
	
	private $logTime = false;
	private $token   = null;
	
	const  INFO    = 'info';
	const  SUCCESS = 'success';
	const  ERROR   = 'error';
	const  WARNING = 'warning';
	const  DEBUG   = 'debug';
	const  STACH   = 'stash';
	const  ALL     = 'all';
	
	private const STATUS = [self::INFO, self::SUCCESS, self::ERROR, self::WARNING, self::DEBUG];
	
	private       $idLog;
	private       $dataLog;
	private       $statusLog;
	private       $keyLog;
	private       $aliasLog;
	private       $jsonDataLog;
	private array $authLevels;
	
	private null|bool|string|\DateInterval $expireLog;
	
	/**
	 * @param $status
	 * @return $this
	 * @throws \Exception
	 */
	public function changeStatus(string $status): static
	{
		if(!in_array($status, self::STATUS) || $this->statusLog === $status){
			return $this;
		}
		
		$this->update(['type' => $status]);
		$this->statusLog = $status;
		return $this;
		
	}
	
	/**
	 * @param $app
	 * @return void
	 */
	public function setApp($app): static
	{
		$this->app = $app;
		return $this;
	}
	
	/**
	 * @param mixed $module
	 */
	public function setModule($module): static
	{
		$this->module = $module;
		return $this;
	}
	
	/**
	 * @param $key
	 * @return $this
	 * @throws \Exception
	 */
	public function key($key)
	{
		$this->update(['key' => $key]);
		$this->keyLog = $key;
		return $this;
	}
	
	/**
	 * @param $alias
	 * @return $this
	 * @throws \Exception
	 */
	public function alias($alias): static
	{
		$this->update(['alias' => $alias]);
		$this->aliasLog = $alias;
		return $this;
	}
	
	public function jsonData($jsonData)
	{
		$this->update(['jsonData' => $jsonData]);
		$this->jsonDataLog = $jsonData;
		return $this;
	}
	
	/**
	 * @param $author
	 * @return void
	 */
	public function setAuthor($author): static
	{
		$this->author = $author;
		return $this;
	}
	
	/**
	 * @param $expire
	 * @return $this
	 * @throws \Exception
	 */
	public function expire(null|bool|string|\DateInterval $expire): static
	{
		$this->expireLog = $expire;
		return $this;
	}
	
	public function setLeveis(string|array $levels): static
	{
		$levels = $levels === self::ALL? self::STATUS: (array) $levels;
		$this->authLevels = array_intersect($levels, self::STATUS);
		return $this;
	}
	
	/**
	 * @param  $new
	 * @return float|string
	 */
	public function logTime($new = false): float
	{
		if($new){
			return $this->logTime = (float) time() . '.' . gettimeofday()['usec'];
		}
		
		$atual = microtime(true); //(float) time() . '.' . gettimeofday()['usec'];
		
		$diferenca = $atual - $this->logTime;
		$diferenca = max(0, $diferenca);
		
		$this->logTime = $atual;
		
		return round($diferenca, 5);
	}
	
	/**
	 * @param $log
	 * @param $data
	 * @param $key
	 * @param $expire
	 * @param $jsonData
	 * @param $alias
	 * @return array
	 */
	protected function prepareLog($log, $data = null, $key = null, $expire = true, $jsonData = null, $alias = null)
	{
		(!$data)?: $this->pushDataLog($data);
		
		$intervaloExpiracao = $this->expireDateInterval($expire);
		$dateExpired = $intervaloExpiracao? (new \DateTime())->add($intervaloExpiracao): null;
		
		return [
			'type'     => $this->statusLog,
			'app'      => $this->app,
			'module'   => $this->module,
			'key'      => $key ?? $this->keyLog,
			'alias'    => $alias ?? $this->aliasLog,
			'log'      => $log,
			'data'     => $this->prepareData(),
			'jsonData' => (array) ($jsonData == null? $this->jsonDataLog ?? []: $jsonData ?? []),
			'token'    => $this->token,
			'author'   => $this->author,
			'expireAt' => $dateExpired,
		];
	}
	
	protected function expireDateInterval(null|bool|string|\DateInterval $expire): ?\DateInterval
	{
		$intervaloExpiracao = $expire ?? $this->expireLog;
		
		if(is_bool($intervaloExpiracao)){
			return (new \DateInterval(($intervaloExpiracao? 'P14D': 'P3M')));
		}
		
		if(is_string($intervaloExpiracao)){
			return (new \DateInterval($intervaloExpiracao));
		}
		
		return $intervaloExpiracao;
	}
	
	/**
	 * @param $log
	 * @return Logger
	 * @throws \Exception
	 */
	protected function registerLog($log): static
	{
		// se o status atual não estiver na lista de status autorizados
		if(!in_array($this->statusLog, $this->authLevels)){
			return $this;
		}
		
		// tiver um registro criado
		if($this->idLog){
			$this->update($log);
			return $this;
		}
		
		// cria o registro do log
		($register = Logs::build($log))->insert();
		$this->idLog = $register['id'];
		return $this;
	}
	
	/**
	 * @return string
	 */
	private function prepareData()
	{
		$currentTime = $this->startTime;
		
		$data = [];
		foreach($this->dataLog as $k => [$microTime, $status, $text]){
			$diff = ($microTime - $currentTime);
			$startDiff = ($microTime - $this->startTime);
			$currentTime = $microTime;
			
			$header = [
				($k + 1),
				($status),
				((new \DateTime('@' . $microTime))->format(self::DATE_FORMAT)),
				($currentTime),
				(number_format($diff, 5)),
				(number_format($startDiff, 5)),
			];
			
			$header = "[" . implode("] [", $header) . "]";
			$data[] = ($header . " ------------------------------ \r\n" . $text);
		}
		
		return empty($data)? null: implode("\r\n\r\n", $data);
	}
	
	public function __toString(): string
	{
		return $this->prepareData();
	}
	
	/**
	 * @return null
	 */
	private function reset()
	{
		$this->dataLog = [];
		$this->statusLog = self::STACH;
		$this->keyLog = null;
		$this->aliasLog = null;
		$this->expireLog = null;
		$this->jsonDataLog = [];
		
		return null;
	}
	
	private function pushDataLog($text)
	{
		$this->dataLog[] = [microtime(true), $this->statusLog, $text];
	}
	
	private function update(array $data)
	{
		if(empty($this->idLog)){
			return;
		}
		
		$data = array_merge($data, ['id' => $this->idLog]);
		Logs::build($data)->update();
	}
	
	
	
	/**
	 * Logs constructor.
	 *
	 * @param bool $time
	 */
	public function __construct($time = false, string|array $levels = self::ALL)
	{
		$this->startTime = microtime(true);
		$this->reset();
		$this->setLeveis($levels);
		$this->logTime = $time? $this->logTime(true): false;
		$this->token = md5(uniqid(rand(), true));
	}
	
	/**
	 * @param      $mensage
	 * @param null $detail
	 * @param null $key
	 * @param bool $expire
	 * @param int  $jsonData
	 * @param null $alias
	 * @return Logger
	 * @throws \Exception
	 */
	public function success($mensage, $detail = null, $key = null, $expire = true, $jsonData = null, $alias = null): static
	{
		$this->statusLog = self::SUCCESS;
		
		$log = $this->prepareLog($mensage, $detail, $key, $expire, $jsonData, $alias);
		
		return $this->registerLog($log);
	}
	
	/**
	 * @param      $mensage
	 * @param null $detail
	 * @param null $key
	 * @param bool $expire
	 * @param int  $jsonData
	 * @param null $alias
	 * @return Logger
	 * @throws \Exception
	 */
	public function error($mensage, $detail = null, $key = null, $expire = true, $jsonData = null, $alias = null): static
	{
		$this->statusLog = self::ERROR;
		
		$log = $this->prepareLog($mensage, $detail, $key, $expire, $jsonData, $alias);
		
		return $this->registerLog($log);
	}
	
	/**
	 * @param      $mensage
	 * @param null $detail
	 * @param null $key
	 * @param bool $expire
	 * @param int  $jsonData
	 * @param null $alias
	 * @return Logger
	 * @throws \Exception
	 */
	public function info($mensage, $detail = null, $key = null, $expire = true, $jsonData = null, $alias = null): static
	{
		$this->statusLog = self::INFO;
		
		$log = $this->prepareLog($mensage, $detail, $key, $expire, $jsonData, $alias);
		
		return $this->registerLog($log);
	}
	
	/**
	 * @param      $mensage
	 * @param null $detail
	 * @param null $key
	 * @param bool $expire
	 * @param int  $jsonData
	 * @param null $alias
	 * @return Logger
	 * @throws \Exception
	 */
	public function warning($mensage, $detail = null, $key = null, $expire = true, $jsonData = null, $alias = null): static
	{
		$this->statusLog = self::WARNING;
		
		$log = $this->prepareLog($mensage, $detail, $key, $expire, $jsonData, $alias);
		
		return $this->registerLog($log);
	}
	
	/**
	 * @param      $mensage
	 * @param null $detail
	 * @param null $key
	 * @param bool $expire
	 * @param int  $jsonData
	 * @param null $alias
	 * @return Logger
	 * @throws \Exception
	 */
	public function debug($mensage, $detail = null, $key = null, $expire = true, $jsonData = null, $alias = null): static
	{
		$this->statusLog = self::DEBUG;
		
		$log = $this->prepareLog($mensage, $detail, $key, $expire, $jsonData, $alias);
		
		return $this->registerLog($log);
		
	}
	
	/**
	 * @param bool $stash
	 * @return $this
	 * @throws \Exception
	 */
	public function stash($stash = true)
	{
		return $this->step($stash);
	}
	
	/**
	 * @param $text
	 * @return $this
	 * @throws \Exception
	 */
	public function step($text)
	{
		$this->pushDataLog($text);
		$this->update(['data' => $this->prepareData()]);
		return $this;
	}
	
}
